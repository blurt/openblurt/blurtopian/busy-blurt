import { isLoggedIn, extractLoginData } from './UserUtil';

/**
 *
 * @returns {boolean}
 */
export function hasCompatibleKeychain() {
  console.log('window', window)
  if (typeof window !== 'undefined') {
    try {
      return (
        window.blurt_keychain &&
        window.blurt_keychain.requestSignBuffer &&
        window.blurt_keychain.requestBroadcast &&
        window.blurt_keychain.requestSignedCall
      );
    } catch (err) {
      console.log('Logging error', err);
    }
  }

}

/**
 *
 * @returns {boolean}
 */
export function isLoggedInWithKeychain() {
    if (!isLoggedIn()) {
        return false;
    }
    const data = localStorage.getItem('autopost2');
    const [
        username,
        password,
        memoWif,
        login_owner_pubkey,
        login_with_keychain,
    ] = extractLoginData(data);
    return !!login_with_keychain;
}
